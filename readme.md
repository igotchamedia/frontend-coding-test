# Frontend Test - Multi frame template

Create a web application utilizing the following APIs

### Weather API (Darksky)

- An API that returns the local weather forecast.
- You can easily configure the location by providing latitude/longitude
- Please refer to [Documentation](https://darksky.net/dev/docs)

### News API

- A simple API which returns a list of news with different topics
- Use https://igotchasignage.prod.i-gotcha.com/api/v1/data_feeds/export/118.json

## Requirements

1. There are **_three main components_**; weather, news and media.
   <br>The way each components are organized is up to you.
   <br>To give you some inspirations, here are the examples of multi frame template, especially the L-frame (L-shaped frame to handle weather/news APIs and the rest of the area for the media rendering)
   <br><br>
   ![](./Equinoxe.png)
   ![](./LeSommet.png)

2. You should periodically recheck the API response to display the latest data. As a guideline, please recheck weather API every 4 hours and news API every 15 mins. When there is a response with an update, please make an appropriate transition in each component to make sure that the update in data is reflected smoothly in front-end.

3. Use one of the following choices; React, Angular or Vue. React is the most preferrable choise but you are not required to use React. We believe that if you are good at one, learning the others is not an issue.

## Optional Requirements

1. Caching the API response to handle offline scenario.
   <br>Even if the internet connection is lost, the template should still display weather and news with the latest responses that were cached. You can use any technologies for caching, such as localStorage, indexedDB etc..

2. Usage of SASS

## Evaluation Criteria

- All Requirements are respected and functional.
- The Html semantic
- The creativity of layout
- The css
- The way the project is structured (ex: components, files structures...)
